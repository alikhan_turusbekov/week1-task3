import java.util.Scanner;

public class Main {

    public static void prime(int a) {
        int b=0;
        for (int i=a-1; i>1; i--) {
            if (a%i==0) { b++; }
        }
        if (b>0) {
            System.out.println("Composite");
        } else {
            System.out.println("Prime");
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        prime(a);
    }
}
